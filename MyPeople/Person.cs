﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPeople
{
    class Person
    {
        private int _id;
        public int id {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public int Age { get; set; }
        public string Male { get; set; }

        public string GetInfo()
        {
            string s = "Информация о персоне: \n";
            s += string.Format("ФИО: {0} {1} {2} \n", LastName, FirstName, MiddleName);
            s += string.Format("Возраст: {0} \n", Age);
            s += string.Format("Пол: {0} \n", Male);
            return s;
        }
    }
}
